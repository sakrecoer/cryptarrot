Deck design kindly provided by http://www.cbdtarot.com/download/ under a Creative Commons by-nc-sa 3.0 license (see http://www.cbdtarot.com/about/share-and-use/)

Leveraging the high entropy crypto PRNG applied to tarots written by Jaromil https://apiroom.net/api/jaromil/tarots